package org.bitbucket.jrsofty.maven.plugins.installer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "innosetup")
public class InnoSetupMojo extends AbstractMojo {

    @Parameter(property = "innoSetupPath", defaultValue = "C:/Program Files (x86)/Inno Setup 5")
    private String innoSetupPath;
    @Parameter(property = "issScriptFilePath", defaultValue = "")
    private String issScriptFilePath = "";
    @Parameter(property = "verbose", defaultValue = "false")
    private boolean verbose = false;
    @Parameter(property = "outputFilePath", defaultValue = "")
    private String outputFilePath = "";
    @Parameter(property = "outputFileName", defaultValue = "")
    private String outputFileName = "";

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        // check that inno setup is located where the configuration says it is.
        this.getLog().info(":::BEGIN INNO SETUP COMPILE:::");
        this.getLog().info(this.innoSetupPath);
        final String fullPathToExe = this.innoSetupPath + (this.innoSetupPath.substring((this.innoSetupPath.length() - 1)).equalsIgnoreCase("/") ? "" : "/") + "ISCC.exe";
        this.getLog().info(fullPathToExe);
        final File executable = new File(fullPathToExe);
        this.getLog().info("executable file should be created");
        if (!executable.exists()) {
            this.getLog().error("Invalid Inno Setup path: " + fullPathToExe + " you must configure the installation path using the 'innoSetupPath' tag.");
            throw new MojoExecutionException("Could not find the Inno Setup command line executable ISCC.exe");
        }
        if (this.issScriptFilePath == null) {
            this.getLog().error("Where's the script file!");
            throw new MojoExecutionException("issScriptFilePath not set");
        }
        // check that the installer scripts are available
        this.getLog().info(this.issScriptFilePath);
        final File script = new File(this.issScriptFilePath);
        this.getLog().info("script file should be created");
        if (!script.exists()) {
            this.getLog().error("Cannot find the script file: " + this.issScriptFilePath);
            throw new MojoExecutionException("Could not find the Inno Setup ISS File.");
        }

        // call the iscc.exe application
        final String[] command = this.buildCommandArray(executable.getAbsolutePath(), script.getAbsolutePath());
        this.getLog().info("Created command string array");
        final ProcessBuilder builder = new ProcessBuilder(command);
        final Process process;
        try {
            process = builder.start();
            final Thread std = new Thread() {
                @Override
                public void run() {
                    InnoSetupMojo.this.getLog().debug("Begin reading output from ISCC");
                    try {
                        final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                        String line = null;

                        while ((line = reader.readLine()) != null) {
                            InnoSetupMojo.this.getLog().info(line);
                        }
                    } catch (final IOException e) {
                        InnoSetupMojo.this.getLog().error(e);
                    }
                    InnoSetupMojo.this.getLog().debug("Complete reading output from ISCC");
                }
            };
            std.start();
            final int result = process.waitFor();
            this.getLog().info("Compile complete: " + result);
        } catch (final IOException e) {
            this.getLog().error(e);
            throw new MojoExecutionException("Failed to create process.", e);

        } catch (final InterruptedException e) {
            this.getLog().error(e);
        }
        this.getLog().info(":::END INNO SETUP COMPILE:::");
    }

    private String[] buildCommandArray(final String commandPath, final String sourcePath) {
        final ArrayList<String> commands = new ArrayList<String>();
        commands.add(commandPath);
        if (!this.verbose) {
            commands.add("/Q");
        }
        if (!this.outputFilePath.isEmpty()) {
            commands.add("/O" + this.outputFilePath);
        }
        if (!this.outputFileName.isEmpty()) {
            commands.add("/F" + this.outputFileName);
        }
        commands.add(sourcePath);
        return commands.toArray(new String[commands.size()]);
    }

    public String getInnoSetupPath() {
        return this.innoSetupPath;
    }

    public void setInnoSetupPath(final String innoSetupPath) {
        this.innoSetupPath = innoSetupPath;
    }

    public String getIssScriptFilePath() {
        return this.issScriptFilePath;
    }

    public void setIssScriptFilePath(final String issScriptFilePath) {
        this.issScriptFilePath = issScriptFilePath;
    }

    public boolean getVerbose() {
        return this.verbose;
    }

    public void setVerbose(final boolean verbose) {
        this.verbose = verbose;
    }

    public String getOutputFilePath() {
        return this.outputFilePath;
    }

    public String getOutputFileName() {
        return this.outputFileName;
    }

    public void setOutputFileName(final String outputFileName) {
        this.outputFileName = outputFileName;
    }

    public void setOutputFilePath(final String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

}
