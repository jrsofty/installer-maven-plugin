# README #


### What is this repository for? ###

* This plugin is for calling various installer package building tool kits for Windows and eventually also Linux. 
* Currently working on version 0.0.1-SNAPSHOT

### Licensing ###

This project uses the MIT open source license. You will find it in the source code in both markdown and plain text file.

### How do I get set up? ###

* TBD

### Contribution guidelines ###

* TBD

### Who do I talk to? ###

* Contact me here or directly via [email](mailto:jrsofty@gmail.com).
